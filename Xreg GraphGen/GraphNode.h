#pragma once

#include <iostream>
#include <vector>


//this struct is a vertex in the graph that counts the number of edges coming out from it
//it also contains a vector of all the connected nodes, and a boolean that is set to true if the node is connected to a graph of more than 0 nodes
struct GraphNode
{
    int mEdgeCount{0};
    bool mConnected{false};
    std::vector<std::shared_ptr<GraphNode>> mConnections;
};